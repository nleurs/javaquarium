new Vue({
	el: "#tableau",
	data: {
		poissons: []
	},
	mounted() {
		fetch("http://localhost:8080/api/poisson")
		.then(response => response.json())
		.then(json => {
			this.poissons = json;
		})
	},
	methods: {
    	deletePoisson: function(id) {
    		let pichons = this.poissons;
    		// DELETE data
            fetch('http://localhost:8080/api/poisson/' + id, {
                method: 'DELETE',
        		headers: {
        			'Content-Type': 'application/json'
    			}
            }).then(function (response) {
            	console.log(response);
            	switch (response.status) {
            	case 500:
            		M.toast({html: "500 Internal Server Error"});
            		break;
            	case 200:
            		let i = pichons.map(item => item.id).indexOf(id) // find index of your object
            		pichons.splice(i, 1) // remove it from array

            		M.toast({html: "Poisson supprimé"});
            		break;
        		default:
            		M.toast({html: "Réponse non prévue: " + response.status});
            	}
            	return response;
            }).catch(error => {
            	console.log(error);
                return err;
            })
    	}
	}
});

new Vue({
	el: "#controls",
	methods: {
    	retour: function() {
    		if ('referrer' in document && document.referrer == "http://localhost:8080/") {
    	        history.back();
    	    } else {
    	    	window.location = '/';
    	    }
    	}
	}
});