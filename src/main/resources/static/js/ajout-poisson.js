new Vue({
    el: '#div-ajout-poisson',
    data: {
        debug: true,
        nom: '',
        description: '',
        couleur: '',
        dimensions: '',
        prix: '',
        doingJob: false
    },
    methods: {
    	retour: function() {
    		if ('referrer' in document && document.referrer == "http://localhost:8080/listerEspeces.do") {
    	        window.location = document.referrer;
    	    } else {
    	    	window.location = '/listerEspeces.do';
    	    }
    	},
    	ajouterPoisson: function() {    		
    		doingJob: true;
    	
    		// check if everything is ok
    		if (!nom.validity.valid || !dimensions.validity.valid || !prix.validity.valid) {
    			M.toast({html: 'Formulaire incorrect'});
    			return;
    		}
    		
    		let json = JSON.stringify({
            	nom: nom.value,
                description: description.value,
                couleur: couleur.value,
                dimensions: dimensions.value,
                prix: prix.value
    		});
    		
    		console.log(json);
    		
    		// POST data
            fetch('http://localhost:8080/api/poisson', {
                method: 'POST',
                body: json,
        		headers: {
        			'Content-Type': 'application/json'
    			}
            }).then(function (response) {
            	console.log(response);
            	switch (response.status) {
            	case 500:
            		M.toast({html: "500 Internal Server Error"});
            		break;
            	case 200:
            		if ('referrer' in document) {
            	        window.location = document.referrer;
            	    } else {
            	    	window.location = '/listerEspeces.do';
            	    }
            		break;
        		default:
            		M.toast({html: "Réponse non prévue: " + response.status});
            	}
            	return response;
            }).catch(err => {
            	console.err(err);
                return err;
            }).then(function () {
            	doingJob = false;
            });
    	}
    }
});
