package fr.univlittoral.javaquarium.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univlittoral.javaquarium.DTO.PoissonDTO;
import fr.univlittoral.javaquarium.service.PoissonService;

/**
 * Controller de l'API poisson
 * @author salledebian
 *
 */
@RestController
@RequestMapping(value = "/api/poisson")
@Component
public class PoissonController {
	
	@Autowired
	private PoissonService service;
	
	/**
	 * Recuperer la liste de tous les poissons
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<PoissonDTO> findAll() {
		return service.findAll();
	}
	
	/**
	 * Recuperer le poisson correspondant à l'id
	 * @param id L'id du poisson
	 * @return Un PoissonDTO ayant l'id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public PoissonDTO findOne(@PathVariable Integer id) {
		return service.findOne(id);
	}
	
	/**
	 * Entrer en base un poisson
	 * @param poisson Le poisson à entrer en base
	 * @return True si le poisson est créé, False sinon
	 */
	@RequestMapping(method = RequestMethod.POST)
	public Boolean create(@RequestBody PoissonDTO poisson) {
		service.create(poisson);
		return true;
	}
	
	/**
	 * Mettre à jour un poisson
	 * @param poisson Le poisson à mettre à jour
	 * @return True si le poisson est update, False sinon
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public Boolean update(@RequestBody PoissonDTO poisson) {
		//TODO update le poisson
		return null;
	}
	
	/**
	 * Effacer un poisson de la base
	 * @param id L'id du poisson a effacer
	 * @return True si le poisson est effacé, False sinon
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Boolean delete(@PathVariable Integer id) {
		service.delete(id);
		return null;
	}
}
