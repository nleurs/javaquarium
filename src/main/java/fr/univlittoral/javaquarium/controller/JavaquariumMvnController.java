package fr.univlittoral.javaquarium.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.univlittoral.javaquarium.JavaquariumApplication;

/**
 * Controller de l'appli principale
 * @author salledebian
 *
 */
@Controller
public class JavaquariumMvnController {
    /**
     * Acces à la page d'accueil
     * @param model Le modele auquel on passera les parametres
     * comme le nom d'appli ou autre.
     * @return La page HTML avec Thymeleaf
     */
    @GetMapping("/")
    public String home(Model model) {
    	model.addAttribute("appName", JavaquariumApplication.appName);
        return "home";

    }
    
    /**
     * Acces à la liste des poissons
     * @return La page HTML avec Thymeleaf
     */
    @GetMapping("/listerEspeces.do")
    public String listerEspeces() {
        return "lister-especes";
    }
    
    /**
     * Acces à la page d'ajout d'un poisson
     * @return La page HTML avec Thymeleaf
     */
    @GetMapping("/ajoutPoisson.do")
    public String ajoutPoisson() {
        return "ajout-poisson";
    }
    
    /**
     * Ne sert qu'à titre d'exemple, car on est un vulgaire noob
     * @param model
     * @param name
     * @return
     */
    @GetMapping("/exemple")
    public String exemple(Model model,
                       @RequestParam(value = "name", required = false,
                               defaultValue = "Guest") String name) {

        model.addAttribute("name", name);
        model.addAttribute("title", "ok");
        return "exemple";
    }
}
