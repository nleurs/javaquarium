package fr.univlittoral.javaquarium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Application Javaquarium (SpringApplication)
 * @author salledebian
 *
 */
@SpringBootApplication
public class JavaquariumApplication{
	// On ne definit le nom de l'appli qu'ici
	public static final String appName = "JavaQuarium";
	
	public static void main(String[] args) {
		SpringApplication.run(JavaquariumApplication.class, args);
	}
}
