package fr.univlittoral.javaquarium.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univlittoral.javaquarium.DO.UtilisateurEntity;

public interface IUtilisateurRepository extends JpaRepository<UtilisateurEntity, Long> {

	UtilisateurEntity findByLoginOrEmail(final String login, final String email);
}
