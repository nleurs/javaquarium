package fr.univlittoral.javaquarium.DO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="P_POISSONS")
public class PoissonDO {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="P_ID")
	private Integer id;
	
	@Column(name="P_ESPECE")
	private String espece;
	
	@Column(name="P_DESC1")
	private String description1;
	
	@Column(name="P_DESC2")
	private String description2;
	
	@Column(name="P_DESC3")
	private String description3;
	
	@Column(name="P_COULEUR")
	private String couleur;
	
	@Column(name="P_LARGEUR")
	private Double largeur;
	
	@Column(name="P_LONGUEUR")
	private Double longueur;
	
	@Column(name="P_PRIX")
	private Double prix;
	
	public PoissonDO() {}

	@Id
	@GenericGenerator(name="kaugen" , strategy="increment")
	@GeneratedValue(generator="kaugen")
	@Column(name="id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEspece() {
		return espece;
	}

	public void setEspece(String espece) {
		this.espece = espece;
	}

	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getDescription3() {
		return description3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public Double getLargeur() {
		return largeur;
	}

	public void setLargeur(Double largeur) {
		this.largeur = largeur;
	}

	public Double getLongueur() {
		return longueur;
	}

	public void setLongueur(Double longueur) {
		this.longueur = longueur;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "PoissonDO [id=" + id + ", espece=" + espece + ", description1=" + description1 + ", description2="
				+ description2 + ", description3=" + description3 + ", couleur=" + couleur + ", largeur=" + largeur
				+ ", longueur=" + longueur + ", prix=" + prix + "]";
	}
	
	
}
