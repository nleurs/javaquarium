package fr.univlittoral.javaquarium.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.DO.UtilisateurEntity;
import fr.univlittoral.javaquarium.repository.IUtilisateurRepository;

@Component
public class UtilisateurBO {
	@Autowired IUtilisateurRepository repository;

	public UtilisateurEntity findByLoginOrEmail(final String login) {
		return repository.findByLoginOrEmail(login, login);
	}
	
}