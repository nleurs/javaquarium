package fr.univlittoral.javaquarium.service;

import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.DTO.UtilisateurDTO;
import fr.univlittoral.javaquarium.config.PortailSpringUser;

@Component
public class UtilisateurMapper {
	public UtilisateurMapper() {}
	
	public UtilisateurDTO map(final PortailSpringUser utilisateur) {
		UtilisateurDTO uDTO = new UtilisateurDTO();
		uDTO.setIdentifiant(utilisateur.getUsername());
		uDTO.setMotDePasse(utilisateur.getPassword());
		return uDTO;
	}
}
