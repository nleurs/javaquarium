package fr.univlittoral.javaquarium.service;

import org.springframework.stereotype.Component;

@Component
public class PasswordBO {

	public boolean matches(String password, String password2) {
		return password.equals(password2);
	}

}
