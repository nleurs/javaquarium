package fr.univlittoral.javaquarium.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.univlittoral.javaquarium.DAO.PoissonDAO;
import fr.univlittoral.javaquarium.DO.PoissonDO;
import fr.univlittoral.javaquarium.DTO.PoissonDTO;

/**
 * Service gérant les conversions PoissonDTO <-> PoissonDO
 * et le PoissonDAO
 * @author salledebian
 *
 */
@Component
public class PoissonService {
	// Le DAO pour l'acces aux poissons
	@Autowired
	private PoissonDAO poissonDAO;
	
	/**
	 * Trouver la liste de tous les poissons
	 * @return La liste de tous les poissons
	 */
	public List<PoissonDTO> findAll() {
		final List<PoissonDTO> list = new ArrayList<>();
		for (final PoissonDO poissonDO : poissonDAO.findAll()) {
			list.add(toPoissonDTO(poissonDO));
		}
		return list;
	}
	
	/**
	 * Trouver le poisson correspondant à l'id
	 * @param id L'id du poisson à trouver
	 * @return Le poisson correspondant à l'id
	 */
	public PoissonDTO findOne(Integer id) {
		return toPoissonDTO(poissonDAO.findOne(id));
	}
	
	/**
	 * Créer le poisson dans la base
	 * @param poissonDTO Poisson à entrer en base
	 */
	public void create(PoissonDTO poissonDTO) {
		poissonDAO.create(toPoissonDO(poissonDTO));
	}
	
	public void delete(Integer id) {
		poissonDAO.remove(id);
	}
	
	/**
	 * Conversion PoissonDO -> PoissonDTO
	 * @param poissonDO PoissonDO à convertir
	 * @return Le PoissonDO
	 */
	private PoissonDTO toPoissonDTO(PoissonDO poissonDO) {
		PoissonDTO poissonDTO = new PoissonDTO();
		poissonDTO.setId(poissonDO.getId());
		poissonDTO.setNom(poissonDO.getEspece());
		poissonDTO.setDescription(poissonDO.getDescription1());
		poissonDTO.setCouleur(poissonDO.getCouleur());
		poissonDTO.setDimensions(String.format("%.2f", poissonDO.getLargeur()) + " x " + String.format("%.2f", poissonDO.getLongueur()));
		poissonDTO.setPrix(poissonDO.getPrix().intValue());
		return poissonDTO;
	}
	
	/**
	 * Conversion PoissonDTO -> PoissonDO
	 * @param poissonDO PoissonDTO à convertir
	 * @return Le PoissonDO
	 */
	private PoissonDO toPoissonDO(PoissonDTO poissonDTO) {
		PoissonDO poissonDO = new PoissonDO();
		poissonDO.setId(poissonDTO.getId());
		poissonDO.setEspece(poissonDTO.getNom());
		poissonDO.setCouleur(poissonDTO.getCouleur());
		
		poissonDO.setDescription1(poissonDTO.getDescription());
		poissonDO.setDescription2("");
		poissonDO.setDescription3("");
		
		String dimension = poissonDTO.getDimensions();
		
		double largeur;
		double longueur;
		String[] dim;
		if (dimension != null && (dim = dimension.split("x")).length == 2) {
			largeur = Double.valueOf(dim[0]);
			longueur = Double.valueOf(dim[1]);
		} else {
			largeur = longueur = 0;
		}
		
		poissonDO.setLargeur(largeur);
		poissonDO.setLongueur(longueur);
		
		double prix;
		Integer prixI = poissonDTO.getPrix();
		if (prixI != null) {
			prix = prixI.doubleValue();
		} else {
			prix = 0;
		}
		poissonDO.setPrix(prix);
		
		return poissonDO;
	}
}
