package fr.univlittoral.javaquarium.DTO;

public class PoissonDTO {
	/**
	 * Attributs de classe
	 */
	private String nom, description, couleur, dimensions;
	private Integer prix, id;
	
	/**
	 * Constructeur par défaut
	 */
	public PoissonDTO() {}
	
	/**
	 * Constructeur
	 * @param id Integer
	 * @param nom String
	 * @param description String
	 * @param couleur String
	 * @param dimensions String au format "largeur x longueur" 
	 * @param prix Integer
	 */
	public PoissonDTO(Integer id, String nom, String description, String couleur, String dimensions, Integer prix) {
		super();
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.couleur = couleur;
		this.dimensions = dimensions;
		this.prix = prix;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCouleur() {
		return couleur;
	}
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	public String getDimensions() {
		return dimensions;
	}
	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}
	public Integer getPrix() {
		return prix;
	}
	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "PoissonDTO [id=" + id + ", nom=" + nom + ", description=" + description + ", couleur=" + couleur + ", dimensions="
				+ dimensions + ", prix=" + prix + "]";
	}
}
