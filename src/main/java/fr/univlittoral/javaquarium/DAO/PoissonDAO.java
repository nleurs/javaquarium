package fr.univlittoral.javaquarium.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.univlittoral.javaquarium.DO.PoissonDO;

@Component
public class PoissonDAO {
	@Autowired
	private EntityManager em;
	
	public List<PoissonDO> findAll() {
		final Query query = em.createQuery("from PoissonDO");
		return query.getResultList();
	}

	public PoissonDO findOne(final Integer id) {
		return em.find(PoissonDO.class, id);
	}
	
	@Transactional
	public void create(PoissonDO poissonDO) {
		em.persist(poissonDO);
	}
	
	public void update(PoissonDO poissonDO) {
		em.merge(poissonDO);
	}
	
	@Transactional
	public void remove(Integer id) {
		em.remove(findOne(id));
	}
}
